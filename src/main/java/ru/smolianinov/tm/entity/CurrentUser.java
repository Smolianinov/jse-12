package ru.smolianinov.tm.entity;

public class CurrentUser {

    private static String currentUser;

    public static String getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(String currentUser) {
        CurrentUser.currentUser = currentUser;
    }


}
