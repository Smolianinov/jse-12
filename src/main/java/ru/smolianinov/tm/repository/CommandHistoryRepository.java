package ru.smolianinov.tm.repository;


import java.util.ArrayList;

public class CommandHistoryRepository {

    private ArrayList<String> history = new ArrayList<>();

    public void addCommandHistory(String command) {
        if (history.size() == 10) history.remove(0);
        history.add(command);
    }

    public int viewCommandHistory() {
        System.out.println("[COMMAND HISTORY]");
        history.forEach((commands) -> System.out.println(commands));
        System.out.println("[END COMMAND HISTORY]");
        return 0;
    }


}