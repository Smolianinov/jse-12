package ru.smolianinov.tm.repository;

import ru.smolianinov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    /**
     * Создать задачу
     *
     * @param name имя задачи
     * @return
     */
    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Создать задачу
     *
     * @param name        имя задачи
     * @param description описание задачи
     * @return
     */
    public final Task create(final String name, String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    /**
     * Изменить задачу
     *
     * @param id          номер задачи
     * @param name        имя задачи
     * @param description описание задачи
     * @return
     */
    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Искать задачу по идентификатору
     *
     * @param index идентификатор задачи
     * @return
     */
    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    /**
     * Искать задачу по имени
     *
     * @param name имя задачи
     * @return
     */
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    /**
     * Искать задачу по номеру
     *
     * @param id порядковый номер задачи
     * @return
     */
    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }


    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    /**
     * Удалить задачу по идентификатору
     *
     * @param id идентификатор заддачи
     * @return
     */
    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по номеру
     *
     * @param index порядковый номер задачи
     * @return
     */
    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public List<Task> findAddByProjectId(final Long projectId) {
        List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    /**
     * Удалить задачу по имени
     *
     * @param name имя задачи
     * @return
     */
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task assignTaskToUser(final String login, final Long id) {
        final Task task = findById(id);
        if (task == null) {
            System.out.println("[PROJECT ID NOT FOUND]");
            return null;
        }
        task.setReponsibleUser(login);
        return task;
    }

    public Task removeTaskFromUser(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setReponsibleUser("");
        return task;
    }

}
