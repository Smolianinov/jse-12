package ru.smolianinov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.smolianinov.tm.entity.User;
import ru.smolianinov.tm.utils.Md5;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private final List<User> users = new ArrayList<>();

    @NotNull
    public final User create(final String login, final String password, final String fio, final String role) {
        final User user = new User();
        user.setLogin(login);
        user.setPassword(Md5.md5Apache(password));
        user.setFio(fio);
        user.setRole(role);
        users.add(user);
        return user;
    }

    /**
     * Искать пользователя по логину
     *
     * @param login логин
     */
    public final User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    /**
     * Искать проект по ФИО
     *
     * @param fio ФИО
     * @return
     */
    public final User findByFio(final String fio) {
        if (fio == null || fio.isEmpty()) return null;
        for (final User user : users) {
            if (user.getFio().equals(fio)) return user;
        }
        return null;
    }

    /**
     * Удалить проект по номеру
     *
     * @param login порядковый номер
     * @return
     */
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User findByName(final String login) {
        if (login == null || login.isEmpty()) return null;
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User updateFio(final String login, final String fio) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setFio(fio);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

    public User updatePassword(final String login, final String password) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

}
