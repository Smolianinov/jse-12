package ru.smolianinov.tm.service;

import ru.smolianinov.tm.entity.Project;
import ru.smolianinov.tm.repository.ProjectRepository;

import java.util.Comparator;
import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository)
    {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    /**
     * Создать проект
     *
     * @param name        имя проекта
     * @param description описание проекта
     * @return
     */
    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    /**
     * Изменить проект
     *
     * @param id          идентификатор проекта
     * @param name        имя проекта
     * @param description описание проекта
     * @return
     */
    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    /**
     * Искать проект по номеру
     *
     * @param index порядковый номер проекта
     * @return
     */
    public Project findByIndex(int index) {
        if (index <= 0) return null;
        return projectRepository.findByIndex(index);
    }

    /**
     * Искать проект по имени
     *
     * @param name имя проекта
     * @return
     */
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    /**
     * Искать проект по  идентификатору
     *
     * @param id идентификатор проекта
     * @return
     */
    public Project findById(Long id) {
        if (id <= 0 || id == null) return null;
        return projectRepository.findById(id);
    }

    /**
     * Удалить проект по номеру
     *
     * @param index порядковый номер проекта
     * @return
     */
    public Project removeByIndex(int index) {
        if (index <= 0) return null;
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удалить проект по идентификатору
     *
     * @param id идентификатор проекта
     * @return
     */
    public Project removeById(Long id) {
        if (id <= 0 || id == null) return null;
        return projectRepository.removeById(id);
    }

    /**
     * Удалить проект по имени
     *
     * @param name имя проекта
     * @return
     */
    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public List<Project> findAll() {
        List<Project> project = projectRepository.findAll();
        project.sort(Comparator.comparing(Project::getName));
        return project;
    }

    public static void main(String[] args) {
        ProjectRepository.main(args);
    }

    public  Project assignProjectToUser(String username, Long id){

        return projectRepository.assignProjectToUser(username, id);
    }

    public  Project removeProjectFromUser(Long id){
        return projectRepository.removeProjectFromUser(id);
    }


}
