package ru.smolianinov.tm.service;

import ru.smolianinov.tm.entity.Project;
import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.repository.TaskRepository;

import java.util.Comparator;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Создать задачу
     *
     * @param name имя задачи
     * @return
     */
    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    /**
     * Создать задачу
     *
     * @param name        имя задачи
     * @param description описание задачи
     * @return
     */
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    /**
     * Изменить задачу
     *
     * @param id          идентификатор задачи
     * @param name        имя задачи
     * @param description описание задачи
     * @return
     */
    public Task update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    /**
     * Искать задачу по номеру
     *
     * @param index порядковый номер задачи
     * @return
     */
    public Task findByIndex(int index) {
        if (index <= 0) return null;
        return taskRepository.findByIndex(index);
    }

    /**
     * Искать задачу по имени
     *
     * @param name имя задачи
     * @return
     */
    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    /**
     * Искать задачу по идентификатору
     *
     * @param id идентификатор задачи
     * @return
     */
    public Task findById(Long id) {
        if (id <= 0 || id == null) return null;
        return taskRepository.findById(id);
    }

    /**
     * Удалить задачу по идентификатору
     *
     * @param id идентификатор задачи
     * @return
     */
    public Task removeById(Long id) {
        if (id <= 0 || id == null) return null;
        return taskRepository.removeById(id);
    }

    /**
     * Удалить задачу по номеру
     *
     * @param index порядковый номер задачи
     * @return
     */
    public Task removeByIndex(int index) {
        if (index <= 0) return null;
        return taskRepository.removeByIndex(index);
    }

    /**
     * Удалить задачу по имени
     *
     * @param name имя задачи
     * @return
     */
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        List<Task> task = taskRepository.findAll();
        task.sort(Comparator.comparing(Task::getName));
        return task;
    }

    public List<Task> findAddByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAddByProjectId(projectId);
    }

    public Task assignTaskToUser(String username, Long id){
        return taskRepository.assignTaskToUser(username, id);
    }

    public  Task removeTaskFromUser(Long id){
        return taskRepository.removeTaskFromUser(id);
    }

}
