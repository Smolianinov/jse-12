package ru.smolianinov.tm;

import ru.smolianinov.tm.controller.ProjectController;
import ru.smolianinov.tm.controller.SystemController;
import ru.smolianinov.tm.controller.TaskController;
import ru.smolianinov.tm.controller.UserController;
import ru.smolianinov.tm.entity.CurrentUser;
import ru.smolianinov.tm.repository.CommandHistoryRepository;
import ru.smolianinov.tm.repository.ProjectRepository;

import ru.smolianinov.tm.repository.TaskRepository;
import ru.smolianinov.tm.repository.UserRepository;
import ru.smolianinov.tm.service.ProjectService;
import ru.smolianinov.tm.service.ProjectTaskService;
import ru.smolianinov.tm.service.TaskService;
import ru.smolianinov.tm.service.UserService;

import java.util.Scanner;

import static ru.smolianinov.tm.constant.TerminalConst.*;

/**
 * Главный класс приложения
 *
 * @author Сергей Смольянинов
 */
public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final ProjectController projectController = new ProjectController(projectService);

    private final UserController userController = new UserController(userService);

    private final SystemController systemController = new SystemController();

    private final CurrentUser currentUser = new CurrentUser();

    private final CommandHistoryRepository commandHistoryRepository = new CommandHistoryRepository();


    //Блок инициализации  для автоматического создания
    //тестовых проектов и задач
    {

        projectRepository.create("DEMO_PROJECT_1");
        projectRepository.create("DEMO_PROJECT_2");
        projectRepository.create("DEMO_PROJECT_3");

        taskRepository.create("DEMO_TASK_1");
        taskRepository.create("DEMO_TASK_2");
        taskRepository.create("DEMO_TASK_3");

        userService.create("ADMIN", "123", "Админов Админ Админович", "ADMIN");
        //System.out.println("Test - " + tstUser.getFio());
        userService.create("TEST", "456", "Тестов Тест Тестович", "USER");
        userService.create("PETROV", "321", "Петров Петр Петрович", "USER");
    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application app = new Application();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Метод выбора дальнейшего события в зависимости от значения поля param
     *
     * @param param - параметр определяющий событие
     */
    public int run(final String param) {

        if (param == null || param.isEmpty()) return -1;
        commandHistoryRepository.addCommandHistory(param);
        switch (param) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.displayExit();

            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();
            case PROJECT_ASSIGN_TO_USER:
                return projectController.assignProjectToUser();
            case PROJECT_REMOVE_FROM_USER:
                return projectController.removeProjectFromUser();
            case PROJECT_VIEW_USER:
                return projectController.viewProject();

            case TASK_CREATE:
                return taskController.createTask();
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskToProjectByIds();
            case TASK_ASSIGN_TO_USER:
                return taskController.assignTaskToUser();
            case TASK_REMOVE_FROM_USER:
                return taskController.removeTaskFromUser();
            case TASK_VIEW_USER:
                return taskController.viewTask();

            case USER_LIST:
                return userController.listUser();
            case USER_CLEAR:
                return userController.clearUser();
            case USER_CREATE:
                return userController.createUser();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();
            case USER_UPDATE_FIO_BY_LOGIN:
                return userController.updateUserFioByLogin();
            case USER_UPDATE_PASSWORD_BY_LOGIN:
                return userController.updateUserPasswordByLogin();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_LOGIN:
                return userController.loginUser();
            case USER_LOGOUT:
                return userController.logoutUser();
            case USER_CHANGE_PASS:
                return userController.changePassUser();

            case HISTORY_VIEW:
                return commandHistoryRepository.viewCommandHistory();

            default:
                return systemController.displayError();
        }

    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}

