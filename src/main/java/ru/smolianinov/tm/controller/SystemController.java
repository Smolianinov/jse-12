package ru.smolianinov.tm.controller;

public class SystemController {

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all project.");
        System.out.println("project-list - Display list of project.");
        System.out.println("project-view-by-index - View project by index.");
        System.out.println("project-view-by-id - View project by id.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println("project-assign-to-user - Assign project to user.");
        System.out.println("project-remove-from-user - Remove project from user.");
        System.out.println("project-view-user - View project user.");

        System.out.println();
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all task.");
        System.out.println("task-list - Display list of task.");
        System.out.println("task-view-by-index - View task by index.");
        System.out.println("task-view-by-id - View task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-id - Update task by id.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println("task-assign-to-user - Assign task to user.");
        System.out.println("task-remove-from-user - Remove task from user.");
        System.out.println("task-view-user - View task user.");

        System.out.println();
        System.out.println("user-create - Create new user.");
        System.out.println("user-list - Display list of user.");
        System.out.println("user-view-by-login - View user by login.");
        System.out.println("project-update-fio-by-login - Update user fio by login.");
        System.out.println("project-update-password-by-login - Update user password by login.");
        System.out.println("user-remove-by-login - Remove user by login.");
        System.out.println("user-login - Login user.");
        System.out.println("user-logout - Logout user.");
        System.out.println("user-change-pass - Change user password.");

        System.out.println("history-view - View last ten command.");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.9");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Sergei Smolianinov");
        System.out.println("sergei@gmail.com");
        return 0;
    }

}
