package ru.smolianinov.tm.controller;

import ru.smolianinov.tm.entity.CurrentUser;
import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.service.ProjectTaskService;
import ru.smolianinov.tm.service.TaskService;

import java.util.List;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int viewTaskById() {
        System.out.println("ENTER, TASK ID:");
        final Long id = scanner.nextLong();
        final Task task = taskService.findById(id);
        viewTask(task);
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        int index = 1;
        viewTask(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASK BY PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAddByProjectId(projectId);
        viewTask(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[PLEASE, ENTER TASK ID]");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskToProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[PLEASE, ENTER TASK ID]");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER TASK NAME]");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        final Long id = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER TASK NAME]");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int assignTaskToUser() {
        System.out.println("ENTER, TASK ID:");
        final Long id = scanner.nextLong();
        if (id == null) {
            System.out.println("[ID TASK CAN'T BE EMPTY]");
            return 0;
        }

        System.out.println("ENTER, USER LOGIN:");
        final String username = scanner.next();
        if (username.isEmpty()) {
            System.out.println("[LOGIN CAN'T BE EMPTY]");
            return 0;
        }

        Task task = taskService.assignTaskToUser(username, id);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[TASK WAS ADDED]");
        }
        return 0;
    }

    public int removeTaskFromUser() {
        System.out.println("ENTER, TASK ID:");
        final Long id = scanner.nextLong();
        if (id == null) System.out.println("[ID TASK CAN'T BE EMPTY]");

        taskService.removeTaskFromUser(id);
        System.out.println("[TASK WAS REMOVED]");
        return 0;
    }

    public int viewTask() {
        final String currentUser = CurrentUser.getCurrentUser();
        if (currentUser == null || currentUser.isEmpty()) {
            System.out.println("[AUTHENTICATE FIRST]");
            return 0;
        }
        if (currentUser.equals("ADMIN")) {
            System.out.println("[LIST TASK]");
            int index = 1;
            for (final Task task : taskService.findAll()) {
                if (!task.getReponsibleUser().equals("")) {
                    System.out.println(index + ". " + task.getName() + " appointed for " + task.getReponsibleUser());
                }
                index++;
            }
        } else {
            System.out.println("[LIST TASK]");
            int index = 1;
            for (final Task task : taskService.findAll()) {
                if (task.getReponsibleUser().equals(currentUser)) {
                    System.out.println(index + ". " + task.getName() + " appointed for " + task.getReponsibleUser());
                }
                index++;
            }
        }
        System.out.println("[OK]");
        return 0;
    }

}
