package ru.smolianinov.tm.constant;

public class TerminalConst {

    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String HELP = "help";
    public static final String EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";
    public static final String PROJECT_ASSIGN_TO_USER = "project-assign-to-user";
    public static final String PROJECT_REMOVE_FROM_USER = "project-remove-from-user";
    public static final String PROJECT_VIEW_USER = "project-view-user";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";
    public static final String TASK_ASSIGN_TO_USER = "task-assign-to-user";
    public static final String TASK_REMOVE_FROM_USER = "task-remove-from-user";
    public static final String TASK_VIEW_USER = "task-view-user";

    public static final String USER_LIST = "user-list";
    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_UPDATE_FIO_BY_LOGIN = "project-update-fio-by-login";
    public static final String USER_UPDATE_PASSWORD_BY_LOGIN = "project-update-password-by-login";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_LOGIN = "user-login";
    public static final String USER_LOGOUT = "user-logout";
    public static final String USER_CHANGE_PASS = "user-change-pass";

    public static final String HISTORY_VIEW = "history-view";
}
