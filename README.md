# Task-manager #
Консольное приложение с возможностью ввода команд. Версия 1.0.9

## Системные требования: ##
  * OpenJDK11

## Стек технологий: ##
  * Apache Maven 3.6.3  

## Разработчик: ##
  * Имя: Смольянинов С.
  * Email: smolianinov@gmail.com
    
## Команды сборки приложения: ##
  `  mvn clean - удаление артефактов  `

  `  mvn compile - компилирование проекта  `

  `  mvn test - тестирование с помощью JUnit тестов  `

  `  mvn package - создание файла jar файла  `

  `  mvn install - копирование файла jar в локальный репозиторий  `

  `  mvn deploy - публикация файла в удаленный репозиторий  `

## Команда запуска приложения: ##
  `  java -jar task-manager-1.0.9.jar  `

## Варианты команд для приложения: ##
  `  help - поcмотреть помощь `

  `  exit - выход из программы  `

  `  version - версия программы  `

  `  project-create - создать проект  `

  `  project-clear - посмотреть проект  `

  `  project-list - посмотреть список проектов  `

  `  project-view-by-index - посмотреть проект по индексу  `
  
  `  project-view-by-id - посмотреть проект по ИД-у `

  `  project-remove-by-name - посмотреть проект по имени `

  `  project-remove-by-id - удалить проект по ИД-у `

  `  project-remove-by-index - удалить проект по индексу `

  `  project-update-by-index - изменить проект  по индексу `

  `  project-update-by-id - изменить проект  по ИД-у `
 
  `  project-assign-to-user - назначить пользователю проект `

  `  project-remove-from-user - удалить у пользователя проект `

  `  project-view-user - посмотреть все проекты пользователя `


  `  task-create - создать задачу  `

  `  task-clear - посмотреть задачу  `

  `  task-list - удалить задачу  `

  `  task-view-by-index - посмотреть задачу по индексу  `
  
  `  task-view-by-id - посмотреть задачу по ИД-у `

  `  task-remove-by-name - посмотреть задачу по имени `

  `  task-remove-by-id - удалить задачу по ИД-у `

  `  task-remove-by-index - удалить задачу по индексу `

  `  task-update-by-index - изменить задачу  по индексу `

  `  task-update-by-id - изменить задачу по ИД-у `

  `  task-assign-to-user - назначить пользователю задачу `

  `  task-remove-from-user - удалить у пользователя задачу `

  `  task-view-user - посмотреть все задачи пользователя `
                                                         

  `  task-list-by-project-id - Показать список задач в проекте `

  `  task-add-to-project-by-ids - Добавить задачу в проект `

  `  task-remove-from-project-by-ids - Удалить задачу из проекта `


  `  user-create - добавить пользователя `

  `  user-list - вывести список пользователей `

  `  user-view-by-login - посмотреть пользователя по логину `

  `  project-update-fio-by-login - изменить ФИО по логину `

  `  project-update-password-by-login - изменить пароль по логину `

  `  user-remove-by-login - удалить пользователя по логину `

  `  user-login - вход(аутентификция) `

  `  user-logout - выход `

  `  user-change-pass - смена пароля `
                                     
                                                           
  `  history-view - Список последних десяти команд пользователя `
